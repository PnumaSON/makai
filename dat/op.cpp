#include "GV.h"


void op1(){

	DrawExtendGraph( 0 , 0 , 640 , 480 , img_board[2] , FALSE ) ;
	
	if(count<128){
		SetDrawBright( count*2 , count*2 , count*2 ) ;
		DrawExtendGraph( 0 , 0 , 640 , 480 ,img_op1[0] ,FALSE) ;
	}
	else if(count<450){
		SetDrawBright( 255 , 255 , 255 ) ;
		DrawExtendGraph( 0 , 0 , 640 , 480 ,img_op1[0] ,FALSE) ;
	}
	else if(count<578){
		SetDrawBright( 1155-2*count , 1155-2*count , 1155-2*count ) ;
		DrawExtendGraph( 0 , 0 , 640 , 480 ,img_op1[0] ,FALSE) ;
	}else if(count<600){
		SetDrawBright( 255 , 255 , 255 ) ;
	}else if(count<710){
		DrawExtendGraph( 0 , 0 , 640, 180 ,img_op1[(count-590)/10] ,TRUE) ;
	}else if(count<750){
		DrawExtendGraph( 0 , 0 , 640, 180 ,img_op1[11] ,TRUE) ;
	}else{
		DrawExtendGraph( 0 , 0 , 640, 180 ,img_op1[11] ,TRUE) ;
		func_state=98;
	}


	//音
	if(count==600){
		PlaySoundMem( sound_se[0] , DX_PLAYTYPE_BACK ) ;
	}

	//飛ばす
	if(CheckStatePad(configpad.start)==1){
		if(count<578){
			count=580;
		}
	}

}

//オープニング
void op2(){
	DrawExtendGraph( 0 , 0 , 640 , 480 , img_board[2] , FALSE ) ;
	DrawExtendGraph( 0 , 0 , 640, 180 ,img_op1[11] ,TRUE) ;

	if(CheckStatePad(configpad.atk)==1){
		switch(opkey){
		case 0:
			func_state=99;
			break;
		case 1:
			func_state=200;
			break;
		case 2:
			DxLib_End();//ＤＸライブラリ終了処理
			break;
		}
	}

	//表示グルグル
	if(CheckStatePad(configpad.up)==1){
		opkey=(opkey+2)%3;
		PlaySoundMem( sound_se[1] , DX_PLAYTYPE_BACK ) ;
	}
	else if(CheckStatePad(configpad.down)==1){
		opkey=(opkey+1)%3;
		PlaySoundMem( sound_se[1] , DX_PLAYTYPE_BACK ) ;
	}	
	
	//選択肢表示
	DrawBox( 220, 240+(70*opkey) , 420, 290+(70*opkey) , color[2] , TRUE ) ;
	DrawExtendGraph( 220 , 240 , 420 , 290 , img_board[4] , TRUE ) ;
	DrawExtendGraph( 220 , 310 , 420 , 360 , img_board[5] , TRUE ) ;
	DrawExtendGraph( 220 , 380 , 420 , 430 , img_board[6] , TRUE ) ;

}

//ライフ０
void restart(){
	DrawExtendGraph( 0 , 0 , 640 , 480 , img_board[9] , FALSE ) ;
	//残機
	DrawGraph( 260 , 350 , img_board[14] , TRUE ) ;
	DrawFormatString( 290, 350, 	color[1], "×%d" ,heart) ;
	if(CheckStatePad(configpad.start)==1 || CheckStatePad(configpad.atk)==1){
		func_state=99;
	}
	StopSoundMem( sound_bgm[0] ) ;
	StopSoundMem( sound_bgm[1] ) ;
}

//ゲームオーバー
void gameover(){
	DrawExtendGraph( 0 , 0 , 640 , 480 , img_board[10] , FALSE ) ;
	if(CheckStatePad(configpad.start)==1 || CheckStatePad(configpad.atk)==1){
		func_state=0;		
	}


}