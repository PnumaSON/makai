#include "GV.h"


void plcontrol(){
	int PMoveX=MoveX;

	MoveX=0;
	MoveY=0;
	PlC_H=4;
	PlC_W=2;
	SFlag--;//SFlag 0:前回しゃがんでいた 1:しゃがんでいる 負:しゃがんでいない

	if(CheckStatePad(configpad.start)==1){
		PFlag=1;
	}
	//無敵状態
	if(TFlag!=0){
		TFlag++;
		if(TFlag<8){
			MoveX=PMoveX;
			return;
		}
	}

	//アタック中
	if(AFlag!=0){
		AFlag++;
		//重力処理
		PlDownSp += G ;
		if(PlDownSp>30){
			PlDownSp=30;
		}

		// 落下速度を移動量に加える
		MoveY = PlDownSp ;
		//空中なら
		if(JFlag!=0){
			MoveX=PMoveX;
		}
		if(AFlag>15){
			AFlag=0;
		}
		return;
	}

	if(CheckStatePad(configpad.down)>0){
		SFlag=1;//しゃがみ中に		
		PlC_H=2;
	}
	else if(CheckStatePad(configpad.up)>0){

	}

	if(CheckStatePad(configpad.left)>0){
		MoveX -=SPEED;
		MUKI=1;
	}
	else if(CheckStatePad(configpad.right)>0){
		MoveX +=SPEED;
		MUKI=0;
	}

	if(CheckStatePad(configpad.atk)==1){
		if(AFlag==0){
			PlaySoundMem( sound_se[4] , DX_PLAYTYPE_BACK ) ;
			AFlag=1;//攻撃中に
		}

	}
	if(CheckStatePad(configpad.jmp)==1){
		if(JFlag==0){
			PlaySoundMem( sound_se[3] , DX_PLAYTYPE_BACK ) ;
			PlDownSp -=JUMP_POWER ;
			JFlag = TRUE ;
		}

	}



	//重力処理
    PlDownSp += G ;
	if(PlDownSp>30){
		PlDownSp=30;
	}

    // 落下速度を移動量に加える
    MoveY = PlDownSp ;

	//無敵解除
	if(TFlag>60){
		TFlag=0;
	}
}


//プレイヤーキャラの移動処理
void CharMove(){

	//確定した移動値を代入
	pldw += MoveX;
	pldh += MoveY;

	//端数処理左右
	if(abs(pldw)>CHIP_SIZE){
		plw += pldw/CHIP_SIZE;
		pldw=0;
	}
	//端数処理上下
	if(abs(pldh)>CHIP_SIZE){
		plh += pldh/CHIP_SIZE;
		pldh=0;
	}

}

//プレイヤーの大きさを考えての当たり判定とか
int HitMap(int kariX,int kariY){
	int i,j;
	int hitw=0,hith=0;//返り値
	int ss=0;
	/*
	for(i=0;i<PlC_H;i++){
		for(j=0;j<PlC_W;j++){
			if(NowMap[plh-i][kariX-j] == 1){//横のみ動いた際の確認
				hitw=1;
			}
			if(NowMap[kariY-i][plw-j] == 1){//縦のみ動いた際の確認
				hith=2;
			}
		}
	}
*/
	//しゃがみ特別処理
	if(SFlag==0){
		ss=1;
	}

	for(i=0;i<PlC_H+ss;i++){
		for(j=0;j<PlC_W;j++){
			if(NowMap[plh-i][kariX-j] == 1 || (pldh!=0&&NowMap[kariY-i][kariX-j] == 1)){
				hitw=1;
			}
			if(NowMap[kariY-i][plw-j] == 1 || (pldw!=0&&NowMap[kariY-i][kariX-j] == 1)){
				hith=10*(i+1);
			}
			if(JFlag==1&&pldw==0&&pldh==0&&NowMap[kariY-i][kariX-j] == 1){
				hitw=1;
			}
		}
	}
	
//	DrawFormatString( 0, 90, GetColor( 255 , 255 , 255 ), "hith= %d",hith ) ;

	return hitw+hith;
	
}


//当たり判定
void plout(){
	int kariX,kariY;//plw plh
	int karidX,karidY;//pldw pldh
	int hit;

	//移動後のpldw
	karidX=pldw+MoveX;
	
	if(karidX>0){
		kariX=plw+1;
	}
	else if(karidX<0){
		kariX=plw-1;
	}else{
		kariX=plw;
	}
	
	//移動後のpldh
	karidY=pldh+MoveY;
	if(karidY>0){
		kariY=plh+1;
	}
	else if(karidY<0){
		kariY=plh-1;
	}else{
		kariY=plh;
	}

	//マップとのあたり
	hit=HitMap(kariX,kariY);
	JFlag=1;
	if((hit%10)==1){//横あたり
		MoveX=0;
	}
	if(10<=hit){//縦あたり
		if((hit/10)==1){//下あたり
			MoveY=0;
			PlDownSp=0;
			JFlag=0;
		}
		if((hit/10)>=PlC_H){//上あたり
			MoveY = 0;
			PlDownSp=0;
			//状態の維持
			if(SFlag==0){
				SFlag=1;
				PlC_H=2;
			}
		}
	}


}

void EnemyToPlayer(int num){
	int hitw=0;

	//距離計算
	//横の計算
	if((CHIP_SIZE*plw+pldw)<(CHIP_SIZE*enemystatus[num].enew+enemystatus[num].enedw)){//エネミーが右
		if((CHIP_SIZE*(enemystatus[num].enew - plw) + (enemystatus[num].enedw - pldw))<CHIP_SIZE*enemystatus[num].EneC_W){
			hitw=-1;
		}
	}else{//エネミーが左
		if((CHIP_SIZE*(plw - enemystatus[num].enew) + (pldw-enemystatus[num].enedw))<CHIP_SIZE*PlC_W){
			hitw=1;
		}
	}

	//縦の計算
	if(hitw!=0){
		if(CHIP_SIZE*plh+pldh<CHIP_SIZE*enemystatus[num].eneh+enemystatus[num].enedh){//エネミーが下
			if((CHIP_SIZE*(enemystatus[num].eneh - plh) + (enemystatus[num].enedh - pldh))<CHIP_SIZE*enemystatus[num].EneC_H){
				if(enemystatus[num].actpattern!=9){//ハートアイテムではない
					MoveX=15*hitw;
					PlDownSp=0;
					TFlag=1;
					life--;
				}else{
					enemystatus[num].life=0;
					if(life<4){
						life++;
					}
				}
			}
		}else{//エネミーが上
			if((CHIP_SIZE*(plh - enemystatus[num].eneh) + (pldh-enemystatus[num].enedh))<CHIP_SIZE*PlC_H){
				if(enemystatus[num].actpattern!=9){//ハートアイテムではない
					MoveX=15*hitw;
					PlDownSp=0;
					TFlag=1;
					life--;
				}else{
					enemystatus[num].life=0;
					if(life<4){
						life++;
					}
				}
			}

		}
	}

}

//プレイヤーの攻撃がエネミーに当たる
void PlayerToEnemy(int num){
	int hitw=0;
	if(MUKI==0){//右向き
		if(CHIP_SIZE*plw+pldw<=CHIP_SIZE*enemystatus[num].enew+enemystatus[num].enedw-CHIP_SIZE*enemystatus[num].EneC_W
			&& CHIP_SIZE*enemystatus[num].enew+enemystatus[num].enedw-CHIP_SIZE*enemystatus[num].EneC_W < CHIP_SIZE*(plw+2)+pldw){

			hitw=1;
		}
	}
	else if(MUKI==1){//左向き
		if(CHIP_SIZE*enemystatus[num].enew+enemystatus[num].enedw<=CHIP_SIZE*plw+pldw-CHIP_SIZE
			&& CHIP_SIZE*plw+pldw-CHIP_SIZE*4<=CHIP_SIZE*enemystatus[num].enew+enemystatus[num].enedw){

			hitw=1;
		}
	}
	
	//縦
	if(hitw!=0){
		if(CHIP_SIZE*plh+pldh<=CHIP_SIZE*enemystatus[num].eneh+enemystatus[num].enedh+CHIP_SIZE*(enemystatus[num].EneC_H)
			&& enemystatus[num].eneh+enemystatus[num].enedh<=CHIP_SIZE*plh+pldh-CHIP_SIZE*(PlC_H-1)){

			enemystatus[num].life -=1;
			enemystatus[num].TFlag=1;
			tumi++;
			if(enemystatus[num].actpattern==9){//ハートなら
				tumi+=3;
			}
		}
	}



}


//敵とプレイヤーのあたり
void HitEnemy(int num){
	//エネミにプレイヤーがぶつかる
	if(TFlag==0){
		EnemyToPlayer(num);
	}
	//プレイヤーの攻撃がエネミーに当たる
	if(AFlag!=0){
		if(enemystatus[num].TFlag==0){
			PlayerToEnemy(num);
		}
	}
}

//ステージ移行判定
void moveboss(){
	if(NowMap[plh][plw] == 3){//もし移行マスなら
		event++;
		func_state=99;
	}

	if(BossF==1 && nowene==0){//ボスが死んだ場合
		event++;
		BossF=0;
		func_state=99;
	}
}

//横スクロール
void game(){
	int i;
	if(PFlag==1){//pausebreak
		SetDrawBright( 125 , 125 , 125 ) ;
		if(CheckStatePad(configpad.start)==1){
			PFlag=0;
			SetDrawBright( 255 , 255 , 255 ) ;
		}
		return;
	}

	//キー入力
	plcontrol();	
	
	//エネミー処理
	enecontrol();

	//当たり判定
	plout();

	//敵とのあたり攻撃とかも
	for(i=0;i<nowene;i++){
			HitEnemy(i);
	}
	//位置の確定
	CharMove();

	//死んだら
	if(life<1){
		func_state=198;
		heart--;
		if(heart<0){//ゲームオーバー
			func_state=199;
		}
	}

	//ボス面への処理
	moveboss();
}