// 文字のサイズ
#define MOJI_SIZE 27

// 仮想テキストバッファの横サイズ縦サイズ
#define STRBUF_WIDTH	20
#define STRBUF_HEIGHT	4

#define MouseLeft		0
#define MouseRight		1

//ゲーム用
#define SCREEN_WIDTH     (640)                          // 画面の横幅
#define SCREEN_HEIGHT    (480)                          // 画面の縦幅
#define CHIP_SIZE        (32)                           // 一つのチップのサイズ
#define CHIP_W			(SCREEN_WIDTH/CHIP_SIZE)
#define CHIP_H			(SCREEN_HEIGHT/CHIP_SIZE)
#define MAP_WIDTH        500							// マップの横幅マックス
#define MAP_HEIGHT       70								// マップの縦幅マックス

#define G                (1)                         // キャラに掛かる重力加速度
#define JUMP_POWER       (24)                         // キャラのジャンプ力
#define SPEED            (5)                         // キャラの移動スピード
#define CHAR_SIZE        (30)                           // プレイヤーのサイズ

#define MAXENE 10 //エネミーの表示限界
