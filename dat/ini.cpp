#include "GV.h"

void firstini(){
	//キーコンフィグの代入
	configpad.down=tempkey[0];
	configpad.left=tempkey[1];
	configpad.right=tempkey[2];
	configpad.up=tempkey[3];
	configpad.atk=tempkey[4];
	configpad.jmp=tempkey[5];
	configpad.slow=tempkey[6];
	configpad.change=tempkey[7];
	configpad.start=tempkey[12];

	//初期
	// 描画位置の初期位置セット
	CursorX = 0 ;
	CursorY = 0 ;
	
	// 参照文字位置をセット
	SP = 0 ;	// １行目の
	CP = 0 ;	// ０文字

	// フレームカウンタ初期化
	KeyWaitFlag = 1;
	count=0;
	opkey=0;


	// 描画する文字列のサイズを設定
	SetFontSize( MOJI_SIZE ) ;
	color[0] =GetColor(255,255,255);
	color[1] =GetColor(255,0,0);
	color[2] =GetColor(0,0,255);
	color[3]=GetColor( 0 , 255 , 0 ) ;

	srand((unsigned) time(NULL));

	//ゲーム内数値の初期化
	life=4;
	heart=4;
	tumi=0;

	event=0;
	stage=0;
	BossF=0;

	//キャラネーム
	strcpy(CharName[0],"");
	strcpy(CharName[1],"ノギリ");	
	strcpy(CharName[2],"イノハラ");
	strcpy(CharName[3],"ドノウエ");
	strcpy(CharName[4],"田嶋");

	//エネミーの大きさ
	EnemyW[4]=2;
	EnemyH[4]=4;
	EnemyW[5]=2;
	EnemyH[5]=4;
	EnemyW[6]=2;
	EnemyH[6]=4;
	EnemyW[7]=2;
	EnemyH[7]=2;
	EnemyW[9]=1;
	EnemyH[9]=1;

	Enemylife[4]=3;
	Enemylife[5]=3;
	Enemylife[6]=3;
	Enemylife[7]=1;

	Enemylife[9]=1;

	StopSoundMem( sound_bgm[0] ) ;
	StopSoundMem( sound_bgm[0] ) ;
}

//ノベル開始前初期化
void novelini(){
	int i,j;
	CP=0;
	EndFlag=0;
	// 仮想テキストバッファを初期化して描画文字位置を初期位置に戻すおよび参照文字位置を一つ進める
	for( i = 0 ; i < STRBUF_HEIGHT ; i ++ )
	{
		for( j = 0 ; j < STRBUF_WIDTH * 2 ; j ++ )
		{
			StringBuf[ i ][ j ] = 0 ;
		}
	}
	// 描画位置の初期位置セット
	CursorX = 0 ;
	CursorY = 0 ;
	// フレームカウンタ初期化
	KeyWaitFlag = 0;
	count=0;
	CharNum=0;
	LRFlag=0;

}

void stagestartini(){
	PFlag=0;
	AFlag=0;
	TFlag=0;
	MFlag=0;
	JFlag=0;
	SFlag=0;
	MUKI=0;
	MoveX=0;
	MoveY=0;
	pldw=0;
	pldh=0;
	PlDownSp=0;
	life=4;
	nowene=0;
}

void bossini(){
	PFlag=0;
	AFlag=0;
	TFlag=0;
	MFlag=0;
	JFlag=0;
	SFlag=0;
	MUKI=0;
	MoveX=0;
	MoveY=0;
	pldw=0;
	pldh=0;
	life=4;
	PlDownSp=0;


	//ボスの設置
	nowene=0;
	enemystatus[nowene].actpattern = stage;//ボスのパターンを
	enemystatus[nowene].MUKI=1;//左向きに
	enemystatus[nowene].enew = 18;
	enemystatus[nowene].eneh = 27;
	enemystatus[nowene].enedh=0;
	enemystatus[nowene].enedw=0;
	enemystatus[nowene].EneC_H = 4;
	enemystatus[nowene].EneC_W = 2;
	enemystatus[nowene].life = 15;
	enemystatus[nowene].actcount=0;
	nowene++;

}

void ini(){
	switch(event){
	case 0://序章イベント
		novelini();
		func_state=100;
		break;
	case 1://一面へ
		stage=1;
		stagestartini();
		mapfileload();
		PlaySoundMem( sound_bgm[0] , DX_PLAYTYPE_LOOP ) ;
		func_state=110;
		break;
	case 2://ボス前イベント
		StopSoundMem( sound_bgm[0] ) ;
		novelini();
		func_state=100;
		break;
	case 3://ボス戦
		PlaySoundMem( sound_bgm[1] , DX_PLAYTYPE_LOOP ) ;
		stage=101;
		BossF=1;
		bossini();
		mapfileload();
		func_state=110;
		break;
	case 4://ボス戦後イベント
		StopSoundMem( sound_bgm[1] ) ;
		novelini();
		func_state=100;
		break;
	case 5://エンド
		func_state=0;
		break;
	}

}