#include "GV.h"


	//エネミーの移動
void eneact(int num){
	enemystatus[num].MoveX=0;
	enemystatus[num].MoveY=0;

	switch(enemystatus[num].actpattern){
		case 4:
			if(	enemystatus[num].MUKI==1){
				enemystatus[num].MoveX-=SPEED/2;
			}
			else if(enemystatus[num].MUKI==0){
				enemystatus[num].MoveX+=SPEED/2;
			}
			//重力処理
			enemystatus[num].PlDownSp += G ;
			if(enemystatus[num].PlDownSp>30){
				enemystatus[num].PlDownSp=30;
			}

			// 落下速度を移動量に加える
			enemystatus[num].MoveY = enemystatus[num].PlDownSp ;
			break;
		case 5:
			if(enemystatus[num].actcount%100==0){
				if(enemystatus[num].JFlag==0){
					enemystatus[num].PlDownSp -=JUMP_POWER ;
					enemystatus[num].JFlag = TRUE ;
				}
			}
			//重力処理
			enemystatus[num].PlDownSp += G ;
			if(enemystatus[num].PlDownSp>30){
				enemystatus[num].PlDownSp=30;
			}

			// 落下速度を移動量に加える
			enemystatus[num].MoveY = enemystatus[num].PlDownSp ;
			
			break;
		case 7://鳥
			if(enemystatus[num].MUKI==1){
				enemystatus[num].MoveX-=SPEED;
			}
			else if(enemystatus[num].MUKI==0){
				enemystatus[num].MoveX+=SPEED;
			}
			if( enemystatus[num].actcount<60){
				enemystatus[num].MoveY+=SPEED/2;
			}
			else if(enemystatus[num].actcount>90){
				enemystatus[num].MoveY-=SPEED/2;
			}
			enemystatus[num].JFlag = TRUE ;
			
			break;

		case 9:
			break;
		case 101://ボス　立ち
			//ランダムで二つの行動
			int tmp;
			enemystatus[num].EneC_H = 4;
			enemystatus[num].EneC_W = 2;
			tmp=rand()%40;

			if(CHIP_SIZE*plw+pldw<CHIP_SIZE*enemystatus[num].enew+enemystatus[num].enedw){//右にエネミ
				enemystatus[num].MUKI=1;
			}else{
				enemystatus[num].MUKI=0;
			}

			if(tmp==0){
				enemystatus[num].actpattern=102;
				enemystatus[num].actcount=0;
				PlaySoundMem( sound_se[10] , DX_PLAYTYPE_BACK ) ;
			}
			else if(tmp==1){
				enemystatus[num].actpattern=104;
				enemystatus[num].actcount=0;
				PlaySoundMem( sound_se[11] , DX_PLAYTYPE_BACK ) ;
			}
			else if(tmp==2){

			}
			//重力処理
			enemystatus[num].PlDownSp += G ;
			if(enemystatus[num].PlDownSp>30){
				enemystatus[num].PlDownSp=30;
			}

			// 落下速度を移動量に加える
			enemystatus[num].MoveY = enemystatus[num].PlDownSp ;
			
			break;
		case 102://ためポーズ
			enemystatus[num].EneC_H = 6;
			enemystatus[num].EneC_W = 2;

			if(CHIP_SIZE*plw+pldw<CHIP_SIZE*enemystatus[num].enew+enemystatus[num].enedw){//右にエネミ
				enemystatus[num].MUKI=1;
			}else{
				enemystatus[num].MUKI=0;
			}
			if(enemystatus[num].actcount>100){
				PlaySoundMem( sound_se[4] , DX_PLAYTYPE_BACK ) ;
				enemystatus[num].actcount=0;
				enemystatus[num].actpattern=103;
			}
			//重力処理
			enemystatus[num].PlDownSp += G ;
			if(enemystatus[num].PlDownSp>30){
				enemystatus[num].PlDownSp=30;
			}

			// 落下速度を移動量に加える
			enemystatus[num].MoveY = enemystatus[num].PlDownSp ;
			break;
		case 103://突っ込み
			enemystatus[num].EneC_H = 2;
			enemystatus[num].EneC_W = 4;

			if(enemystatus[num].actcount<60){
				if(enemystatus[num].MUKI==0){
					enemystatus[num].MoveX += 15;
				}else{
					enemystatus[num].MoveX -= 15;
				}
			}else{
				enemystatus[num].actpattern=101;
				enemystatus[num].actcount=0;
			}
			//重力処理
			enemystatus[num].PlDownSp += G ;
			if(enemystatus[num].PlDownSp>30){
				enemystatus[num].PlDownSp=30;
			}

			// 落下速度を移動量に加える
			enemystatus[num].MoveY = enemystatus[num].PlDownSp ;
			break;

		case 104://ジャンプ切り
			enemystatus[num].EneC_H = 4;
			enemystatus[num].EneC_W = 4;

			if(enemystatus[num].actcount<66){
				if(enemystatus[num].MUKI==0){
					enemystatus[num].MoveX += 5;
				}else{
					enemystatus[num].MoveX -= 5;
				}

				if(	enemystatus[num].actcount==1){
					enemystatus[num].PlDownSp -= 30 ;
				}
			}
			if(enemystatus[num].actcount==160){
				enemystatus[num].actpattern=101;
				enemystatus[num].actcount=0;
			}
			
			//重力処理
			enemystatus[num].PlDownSp += G ;
			if(enemystatus[num].PlDownSp>30){
				enemystatus[num].PlDownSp=30;
			}

			// 落下速度を移動量に加える
			enemystatus[num].MoveY = enemystatus[num].PlDownSp ;
			break;

		default:
			break;
	}
	//actcountの増加
	enemystatus[num].actcount++;
	if(enemystatus[num].TFlag!=0){
		enemystatus[num].TFlag++;
	}
	if(BossF!=1){
		if(enemystatus[num].TFlag>15){
			enemystatus[num].TFlag=0;
		}
	}else{
		if(enemystatus[num].TFlag>50){
			enemystatus[num].TFlag=0;
		}
	}
}