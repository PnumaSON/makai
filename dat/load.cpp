#include "GV.h"


//キー設定ファイルのロード
void keyfileload(){
	FILE *fp ;
	int i;
	char temp[3];
	//もしファイルがなければ
	if( (fp=fopen( "key.dat","rb" ))==NULL ){
		for(i=0;i<14;i++){
			tempkey[i]=i;
		}
	}
	else{
		for(i=0;i<14;i++){
			fgets(temp,6,fp);
			tempkey[i]=atoi(temp);
		}
		fclose( fp ) ;
	}

}
//マップファイルのロード
void mapfileload(){
	int fp ;
	char filename[50];
	int i,j;
	char inputc[2];
	int input[2];

	sprintf(filename,"./data/map/map%d.dat",stage);
	if((fp=FileRead_open(filename))==NULL){
		DrawFormatString( 0, 20, color[0], "%s read error",filename ) ;
	}

    //FileRead_scanf(fp,"%d %d\n",&h,&w);

	for(i=0;i<MAP_HEIGHT;i++){
		for(j=0;j<MAP_WIDTH;j++){
			inputc[0]=input[0]=FileRead_getc(fp);
			if(input[0]=='\n'||input[0]==EOF){
				MapW=j;
				break;
			}
			else if(input[0]=='2'){
				plh=i;plw=j;
			}
			inputc[1]='\0';
			NowMap[i][j]=atoi(inputc);
		}
		if(input[0]==EOF){
			MapH=i+1;
			break;
		}
	}
/*	//デバッグ
	ClearDrawScreen();
	SetFontSize( 10 ) ;
	for(i=0;i<h;i++){
		for(j=0;j<w;j++){
			DrawFormatString( j*10, i*10, color[0], "%d",NowMap[i][j] ) ;
		}
	}
	ScreenFlip();//裏画面反映
*/
	FileRead_close(fp);

}


void load(){

	char tmp[25];
	int i;

	img_op1[0] = LoadGraph("data/op/attention.png");
	for(i=1;i<12;i++){
		sprintf(tmp,"data/op/LOGO2boke%d.png",i);
		img_op1[i] = LoadGraph(tmp);
	}

	img_board[1] = LoadGraph("data/op/black.png");
	img_board[2] = LoadGraph("data/op/maoh.png");
	img_board[3] = LoadGraph("data/op/LOGO.png");
	img_board[4] = LoadGraph("data/op/start.png");
	img_board[5] = LoadGraph("data/op/config.png");
	img_board[6] = LoadGraph("data/op/exit.png");

	//メッセボックス
	img_board[7]	= LoadGraph( "data/novel/messe.png" );
	img_board[8]	= LoadGraph( "data/novel/messeao.png" );

	img_board[9] = LoadGraph("data/op/restart.png");
	img_board[10] = LoadGraph("data/op/gameover.png");

	//ステータス
	img_board[11] = LoadGraph("data/dot/heartwin.png");
	img_board[12] = LoadGraph("data/dot/heart.png");
	img_board[13] = LoadGraph("data/dot/heart2.png");
	img_board[14] = LoadGraph("data/dot/hito.png");
	//キャラ立ち絵
	img_char[1]		= LoadGraph( "data/char/cha1.png" );
	img_char[2]		= LoadGraph( "data/char/cha2.png" );
	img_char[3]		= LoadGraph( "data/char/cha3.png" );
	//どっと
	dot_pl[0]		= LoadGraph( "data/dot/player.png" );
	dot_pl[1]		= LoadGraph( "data/dot/pls.png" );
	dot_pl[2]		= LoadGraph( "data/dot/playera1.png" );

	dot_ene[4][0]	= LoadGraph( "data/dot/enemy4-1.png" );
	dot_ene[5][0]	= LoadGraph( "data/dot/enemy4-1.png" );

	dot_ene[7][0]	= LoadGraph( "data/dot/enemy7.png" );
	dot_ene[9][0]	= LoadGraph( "data/dot/heart.png" );

	//1面
	dot_boss[1][0]	= LoadGraph( "data/dot/boss1-0.png" );
	dot_boss[1][1]	= LoadGraph( "data/dot/boss1-1.png" );
	dot_boss[1][2]	= LoadGraph( "data/dot/boss1-2.png" );
	dot_boss[1][3]	= LoadGraph( "data/dot/boss1-3.png" );

	//音楽
	sound_se[0] = LoadSoundMem("data/se/oplogo.wav") ;
	sound_se[1] = LoadSoundMem("data/se/pu.wav") ;
	sound_se[2] = LoadSoundMem("data/se/pi.wav") ;
	sound_se[3] = LoadSoundMem("data/se/poin.wav") ;
	sound_se[4] = LoadSoundMem("data/se/shu.wav") ;
	sound_se[10] = LoadSoundMem("data/se/gyuuu.wav") ;
	sound_se[11] = LoadSoundMem("data/se/jaki.wav") ;

	sound_bgm[0] = LoadSoundMem("data/sound/sentou.wav") ;
	sound_bgm[1] = LoadSoundMem("data/sound/sakaduki.wav") ;
	//キーコンフィグ
	keyfileload();
}
