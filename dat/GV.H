#include "DxLib.h"
#include "struct.h"
#include "define.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#ifdef GLOBAL_INSTANCE
#define GLOBAL
#else
#define GLOBAL extern 
#endif

#include "function.h"           //関数宣言

#define PI	3.1415926535897932384626433832795f
//ふらぐ
GLOBAL int func_state,count;
GLOBAL int opkey;//opのカーソルの状態
//音楽ファイル用変数宣言部
GLOBAL int sound_se[30];
GLOBAL int sound_bgm[10];
GLOBAL int voice[30];
//画像用変数宣言部
GLOBAL int img_board[40];	//ボードにつかう画像
GLOBAL int img_op1[15];
GLOBAL int img_op2[15];
GLOBAL int img_char[20];
GLOBAL int dot_pl[10];
GLOBAL int dot_ene[10][10];
GLOBAL int dot_boss[5][20];

GLOBAL configpad_t configpad;//コンフィグで設定したキー情報
GLOBAL int tempkey[14];


GLOBAL int MouseX,MouseY;
GLOBAL int color[10];
//novel用
GLOBAL char StringBuf[ STRBUF_HEIGHT ][ STRBUF_WIDTH * 2 + 1 ] ;	// 仮想テキストバッファ
GLOBAL int CursorX , CursorY ;						// 仮想画面上での文字表示カーソルの位置
GLOBAL int SP , CP ,BP;							// 参照する文字列番号と文字列中の文字ポインタ、何ワード目か
GLOBAL int EndFlag ;							// 終了フラグ
GLOBAL int KeyWaitFlag ;						// ボタン押し待ちフラグ
GLOBAL int CharNum;//現在話してるキャラ
GLOBAL int LRFlag;//名前と画像を右に表示か左に表示か　0:左
GLOBAL char CharName[10][256];

//ゲーム内変数
GLOBAL int BossF;
GLOBAL int life,heart,deflife,defheart,tumi;
GLOBAL int NowMap[MAP_HEIGHT][MAP_WIDTH];
GLOBAL int MapH,MapW;
GLOBAL int MoveX,MoveY;//移動距離保存
GLOBAL int MUKI;//向いている方向 右：0　左：1
GLOBAL int plh,plw;//プレイヤ位置整数値(チップ換算)
GLOBAL int pldh,pldw;//プレイヤ位置端数(チップ未満)
GLOBAL int event,stage;
GLOBAL int JFlag,SFlag,AFlag,MFlag,TFlag;//ジャンプ中 しゃがみ中 攻撃中 移動中 無敵時間中
GLOBAL int PFlag;//スタートボタンでストップ
GLOBAL int PlDownSp;//落下速度 
GLOBAL int PlC_H,PlC_W;//現在のプレイヤキャラの横幅と縦幅(チップ換算)　通常縦4横2
GLOBAL int nowene;//現在のエネミーの数
GLOBAL status enemystatus[MAXENE];

GLOBAL int EnemyW[10],EnemyH[10];//エネミーの初期の大きさ
GLOBAL int Enemylife[10];//エネミーのライフ値
