//パッドに関する構造体
typedef struct{
	int key[16];
}pad_t;

//コンフィグに関する構造体
typedef struct{
	int left,up,right,down,atk,jmp,slow,start,change;
}configpad_t;

typedef struct{
	int ReX,ReY;
	int MoveX,MoveY;//移動距離保存
	int eneh,enew,enedh,enedw;
	int EneC_H,EneC_W;
	int PlDownSp;//落下速度 
	int MUKI;
	int JFlag,AFlag,MFlag,TFlag;//ジャンプ中 攻撃中 移動中 無敵時間中
	int actpattern;//行動パターン
	int life;
	int actcount;//アクト用タイム制御
}status;
