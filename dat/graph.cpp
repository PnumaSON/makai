#include "GV.h"


void mapview(){
	int i, j ;


	//画面幅＋両サイド2ずつ描画 playerが中心に来るように
    for( i =  -2 ; i < CHIP_H+2; i ++ )
    {
		//はみ出た場合
		if(i+plh-CHIP_H/2-4<0 || i+plh-CHIP_H/2-4 >=MAP_HEIGHT){
			continue;
		}

        for( j = -2 ; j < CHIP_W+2 ; j ++ )
        {
			//はみ出た場合
			if(j+plw-CHIP_W/2<0 || j+plw-CHIP_W/2 >=MAP_WIDTH){
				continue;
			}
            // １は当たり判定チップを表しているので１のところだけ描画
            if( NowMap[i+plh-CHIP_H/2-4][j+plw-CHIP_W/2] == 1 )
            {
                DrawBox( j * CHIP_SIZE - pldw,                i * CHIP_SIZE - pldh,
                         j * CHIP_SIZE + CHIP_SIZE - pldw,    i * CHIP_SIZE + CHIP_SIZE - pldh,
                         color[0], TRUE ) ;
            }
           // if( NowMap[i+plh-CHIP_H/2-4][j+plw-CHIP_W/2] == 2 )
           // {
           //     DrawBox( j * CHIP_SIZE - pldw,                i * CHIP_SIZE - pldh,
           //              j * CHIP_SIZE + CHIP_SIZE - pldw,    i * CHIP_SIZE + CHIP_SIZE - pldh,
          //               color[2], TRUE ) ;
           // }
			//敵のリスポーン地
            //if( NowMap[i+plh-CHIP_H/2-4][j+plw-CHIP_W/2] == 7 )
           // {
           //     DrawBox( j * CHIP_SIZE - pldw,                i * CHIP_SIZE - pldh,
            //             j * CHIP_SIZE + CHIP_SIZE - pldw,    i * CHIP_SIZE + CHIP_SIZE - pldh,
           //              color[3], TRUE ) ;
            //}

        }
    }

}

void playerview(){

	if(AFlag!=0){//攻撃時
	//	DrawGraph(SCREEN_WIDTH/2 - CHIP_SIZE, (SCREEN_HEIGHT+CHIP_SIZE)/2,dot_pl[2],TRUE);
		DrawRotaGraph( SCREEN_WIDTH/2+CHIP_SIZE*PlC_W/2 -MUKI*CHIP_SIZE*PlC_W, (SCREEN_HEIGHT+CHIP_SIZE)/2 + CHIP_SIZE*PlC_H/2 , 1.0f , 0.0 , dot_pl[2] , TRUE ,MUKI) ;
	}
	else if(0<SFlag){//しゃがみ時
	//	DrawGraph(SCREEN_WIDTH/2 - CHIP_SIZE, (SCREEN_HEIGHT+CHIP_SIZE)/2 + CHIP_SIZE*2,dot_pl[1],TRUE);
		DrawRotaGraph( SCREEN_WIDTH/2, (SCREEN_HEIGHT+CHIP_SIZE)/2 + CHIP_SIZE*(2+PlC_H/2) , 1.0f , 0.0 , dot_pl[1] , TRUE ,MUKI) ;
	}
	else{//通常時
	//	DrawGraph(SCREEN_WIDTH/2 - CHIP_SIZE, (SCREEN_HEIGHT+CHIP_SIZE)/2,dot_pl[0],TRUE);
		DrawRotaGraph( SCREEN_WIDTH/2, (SCREEN_HEIGHT+CHIP_SIZE)/2 + CHIP_SIZE*PlC_H/2 , 1.0f , 0.0 , dot_pl[0] , TRUE ,MUKI) ;
	}
	//仮主人公表示
	//DrawBox( SCREEN_WIDTH/2,				(SCREEN_HEIGHT+CHIP_SIZE*7)/2,
    //         SCREEN_WIDTH/2 + CHIP_SIZE,    (SCREEN_HEIGHT+CHIP_SIZE*7)/2+ CHIP_SIZE,
    //         color[1], TRUE ) ;

}

void enemyview(int num){
//	DrawGraph(SCREEN_WIDTH/2 - CHIP_SIZE + CHIP_SIZE*(enemystatus[num].enew-plw)+enemystatus[num].enedw-pldw, 
//		     (SCREEN_HEIGHT+CHIP_SIZE)/2 + CHIP_SIZE*(enemystatus[num].eneh-plh)+enemystatus[num].enedh-pldh,dot_ene[7][0],FALSE);

	if(BossF!=1){
		DrawRotaGraph( SCREEN_WIDTH/2 - CHIP_SIZE + CHIP_SIZE*(enemystatus[num].enew-plw)+enemystatus[num].enedw-pldw + CHIP_SIZE*enemystatus[num].EneC_W/2,
			(SCREEN_HEIGHT+CHIP_SIZE)/2 + CHIP_SIZE*(enemystatus[num].eneh-plh)+enemystatus[num].enedh-pldh + CHIP_SIZE*(4-enemystatus[num].EneC_H/2), 
			1.0f , 0.0 , dot_ene[enemystatus[num].actpattern][0] , TRUE ,enemystatus[num].MUKI) ;
	}else{//ボス用
		DrawRotaGraph( SCREEN_WIDTH/2 - CHIP_SIZE + CHIP_SIZE*(enemystatus[num].enew-plw)+enemystatus[num].enedw-pldw + CHIP_SIZE*(2-enemystatus[num].EneC_W/2),
			(SCREEN_HEIGHT+CHIP_SIZE)/2 + CHIP_SIZE*(enemystatus[num].eneh-plh)+enemystatus[num].enedh-pldh + CHIP_SIZE*(4-enemystatus[num].EneC_H/2), 
			1.0f , 0.0 , dot_boss[stage-100][enemystatus[num].actpattern-stage] , TRUE ,enemystatus[num].MUKI) ;
	}

}

void statusview(){
	int i;
	DrawGraph( 5 , 5 , img_board[11], TRUE ) ;
	//ハート系
	for(i=0;i<4;i++){
		DrawGraph( i*40+10 , 10 , img_board[13] , TRUE ) ;
	}
	for(i=0;i<life;i++){
		DrawGraph( i*40+10 , 10 , img_board[12] , TRUE ) ;
	}

	//残機
	DrawGraph( 550 , 10 , img_board[14] , TRUE ) ;
	DrawFormatString( 580, 10, 	color[1], "×%d" ,heart) ;


}

//基本レンダー
void view(){
	int i;
	//マップの表示
	mapview();

	//プレイヤの表示
	playerview();

	//エネミーの表示
	for(i=0;i<nowene;i++){
		enemyview(i);
	}

	//残機とか
	statusview();

	if(PFlag==1){//pausebreak
		SetDrawBright( 255 , 255 , 255 ) ;
		DrawFormatString( 290, 240, GetColor( 255 , 255 , 255 ), "Pause" ) ;
		SetDrawBright( 125 , 125 , 125 ) ;
	}
}

void debug(){
	DrawFormatString( 0, 10, GetColor( 255 , 255 , 255 ), "CharNum= %d opkey= %d",CharNum,opkey ) ;
	DrawFormatString( 0, 40, GetColor( 255 , 255 , 255 ), "plw= %d plh= %d",plw,plh ) ;
	DrawFormatString( 0, 70, GetColor( 255 , 255 , 255 ), "pldw= %d pldh= %d",pldw,pldh ) ;
	DrawFormatString( 0, 100, color[1], "nowene=%d %d %d",nowene,CHIP_H,CHIP_W) ;
	DrawFormatString( 0, 400, 	color[1], "ene1 %d" ,enemystatus[0].life) ;
	DrawFormatString( 0, 440, 	color[1], "TFlag= %d" ,TFlag) ;
}
