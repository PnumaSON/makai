#include "GV.h"


void eneini(int i,int n){
	if(n==0){//左
		enemystatus[nowene].ReX = (-2+plw-CHIP_W/2);
		enemystatus[nowene].ReY = i+plh-CHIP_H/2-4;
		enemystatus[nowene].actpattern = NowMap[i+plh-CHIP_H/2-4][-2+plw-CHIP_W/2];
		enemystatus[nowene].MUKI=0;//右向きに
	}
	else if(n==1){//右
		enemystatus[nowene].ReX = (CHIP_W+1+plw-CHIP_W/2);
		enemystatus[nowene].ReY = i+plh-CHIP_H/2-4;
		enemystatus[nowene].actpattern = NowMap[i+plh-CHIP_H/2-4][CHIP_W+1+plw-CHIP_W/2];
		enemystatus[nowene].MUKI=1;//左向きに
	}
	enemystatus[nowene].enew = enemystatus[nowene].ReX;
	enemystatus[nowene].eneh = enemystatus[nowene].ReY;
	enemystatus[nowene].enedh=0;
	enemystatus[nowene].enedw=0;
	enemystatus[nowene].EneC_H = EnemyH[enemystatus[nowene].actpattern];
	enemystatus[nowene].EneC_W = EnemyW[enemystatus[nowene].actpattern];
	enemystatus[nowene].life = Enemylife[enemystatus[nowene].actpattern];
	enemystatus[nowene].actcount=0;
	nowene++;//敵の数を増やす
}

//エネミーリスポーン
void respawn(){
	int i, j ;
	int kf=0;//被りフラグ

	//画面外の位置でリスポーン
    for( i =  -2 ; i < CHIP_H+2; i ++ )
    {
		if(i+plh-CHIP_H/2-4<0 || i+plh-CHIP_H/2-4 >= MAP_HEIGHT){
			continue;
		}
		//敵のリスポーン地があったら
        if( 3<NowMap[i+plh-CHIP_H/2-4][-2+plw-CHIP_W/2] ){//左
			for(j=0;j<nowene;j++){//登場エネミーと被ってないか確認
				if(enemystatus[j].ReX == (-2+plw-CHIP_W/2) || enemystatus[j].actcount<10){//被っていたら
					kf=1;
				}
			}
			if(kf!=1){
				eneini(i,0);
			}

        }

		kf=0;
		
		if( 3<NowMap[i+plh-CHIP_H/2-4][CHIP_W+1+plw-CHIP_W/2] ){//右
			for(j=0;j<nowene;j++){//登場エネミーと被ってないか確認
				if(enemystatus[j].ReX == (CHIP_W+1+plw-CHIP_W/2) || enemystatus[j].actcount<10){
					kf=1;
				}
			}
			if(kf!=1){
				eneini(i,1);
			}
        } 
		
    }

}


//エネミーの大きさを考えての当たり判定とか
int EneHitMap(int kariX,int kariY,int num){
	int i,j;
	int hitw=0,hith=0;//返り値

	for(i=0;i<enemystatus[num].EneC_H;i++){
		for(j=0;j<enemystatus[num].EneC_W;j++){
			if(NowMap[enemystatus[num].eneh-i][kariX-j] == 1 || (enemystatus[num].enedh!=0&&NowMap[kariY-i][kariX-j] == 1)){
				hitw=1;
			}
			if(NowMap[kariY-i][enemystatus[num].enew-j] == 1 || (enemystatus[num].enedw!=0&&NowMap[kariY-i][kariX-j] == 1)){
				hith=10*(i+1);
			}
			if(enemystatus[num].JFlag==1&&enemystatus[num].enedw==0&&enemystatus[num].enedh==0&&NowMap[kariY-i][kariX-j] == 1){
				hitw=1;
			}
		}
	}
	
	//DrawFormatString( 0, 90, GetColor( 255 , 255 , 255 ), "hith= %d",hith ) ;

	return hitw+hith;
	
}
//エネミーのマップとのあたり判定
void eneout(int num){
	int kariX,kariY;//plw plh
	int karidX,karidY;//pldw pldh
	int hit;

	//移動後のpldw
	karidX=enemystatus[num].enedw+enemystatus[num].MoveX;
	
	if(karidX>0){
		kariX=enemystatus[num].enew+1;
	}
	else if(karidX<0){
		kariX=enemystatus[num].enew-1;
	}else{
		kariX=enemystatus[num].enew;
	}
	
	//移動後のpldh
	karidY=enemystatus[num].enedh+enemystatus[num].MoveY;
	if(karidY>0){
		kariY=enemystatus[num].eneh+1;
	}
	else if(karidY<0){
		kariY=enemystatus[num].eneh-1;
	}else{
		kariY=enemystatus[num].eneh;
	}

	//マップとのあたり
	hit=EneHitMap(kariX,kariY,num);
	enemystatus[num].JFlag=1;
	if((hit%10)==1){//横あたり
		enemystatus[num].MoveX=0;
	}
	if(10<=hit){//縦あたり
		if((hit/10)==1){//下あたり
			enemystatus[num].MoveY=0;
			enemystatus[num].PlDownSp=0;
			enemystatus[num].JFlag=0;
		}
		if((hit/10)>=enemystatus[num].EneC_H){//上あたり
			enemystatus[num].MoveY = 0;
			enemystatus[num].PlDownSp=0;
		}
	}
}

//エネミーキャラの移動処理
void EneCharMove(int num){

	//確定した移動値を代入
	enemystatus[num].enedw += enemystatus[num].MoveX;
	enemystatus[num].enedh += enemystatus[num].MoveY;

	//端数処理左右
	if(abs(enemystatus[num].enedw)>CHIP_SIZE){
		enemystatus[num].enew += enemystatus[num].enedw/CHIP_SIZE;
		enemystatus[num].enedw=0;
	}
	//端数処理上下
	if(abs(enemystatus[num].enedh)>CHIP_SIZE){
		enemystatus[num].eneh += enemystatus[num].enedh/CHIP_SIZE;
		enemystatus[num].enedh=0;
	}

}


//デリートのための詰め替え
void tumekae(int num){
	int i;
	for(i=num;i<nowene-1;i++){
		enemystatus[i] = enemystatus[i+1];
	}

}

//エネミーのデリート
void enedel(){
	int num;
	int delflag=0;

	for(num=0;num<nowene;num++){
		delflag=0;
		//はみ出た場合
		if(BossF!=1){//ボス戦じゃないなら
			if(abs(enemystatus[num].enew-plw)>CHIP_W/2+5 || abs(enemystatus[num].eneh-plh) > CHIP_H/2+5){
				delflag=1;	
			}
		}
		//ライフが0の場合
		if(enemystatus[num].life<1){
			delflag=1;
		}

		//デリート
		if(delflag==1){
			tumekae(num);
			nowene--;
			num--;
		}
	}
}



//エネミーの行動処理
void eneset(int num){

	//エネミーの移動
	eneact(num);
	//マップとのあたり
	eneout(num);
	//位置の確定
	EneCharMove(num);

}








//エネミー制御
void enecontrol(){
	int i;
	//デリートの確認
	enedel();

	//リスポーンの確認
	if(nowene<MAXENE-1){
		respawn();
	}

	//存在するエネミーの行動
	for(i=0;i<nowene;i++){
		eneset(i);
	}

}

