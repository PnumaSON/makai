狂火魔界ノ原　ver１　
製作者　PnumaSON

このくだらないゲームをダウンロードいただき大変恐縮です。
こころを広く保ちながら、罵倒しつつゲームしていただければ光栄です。

操作法
ｚ：攻撃
ｘ：ジャンプ
Space：Pause
←→キー：移動
↓キー：しゃがみ

バグが多数混在しておりますがそれは仕様です。
あまりにまずそうなバグがあれば伝えていただければ善処いたします。

著作権等
PNG読み込みによるライセンス
　　　libpng　Copyright (c) 1998-2011 Glenn Randers-Pehrson.
　　　zlib　Copyright (C) 1995-2010 Jean-loup Gailly and Mark Adler.
DXライブラリ
　　　DX Library Copyright (C) 2001-2013 Takumi Yamada.